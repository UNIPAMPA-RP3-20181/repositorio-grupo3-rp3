/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import modelo.BD.UsuarioBD;
import Modelo.entidade.Usuario;
import java.sql.SQLException;

/**
 *
 * @author vagne
 */
public class ControleUsuario {

    UsuarioBD usuarioBD;
    
    public ControleUsuario() {
        this.usuarioBD = new UsuarioBD();
    }

    public Usuario autenticarUsuario(String nomeLogin, String senhaLogin) throws SQLException {
        return usuarioBD.recuperaUsuario(nomeLogin, senhaLogin);
    }

    public String cadastrarUsuario(Usuario usuario) throws SQLException {
        return usuarioBD.inserirBanco(usuario);
    }
}
