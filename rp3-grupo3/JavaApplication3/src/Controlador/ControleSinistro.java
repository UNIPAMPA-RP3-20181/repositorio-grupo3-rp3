/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import modelo.BD.ApoliceBD;
import modelo.BD.SinistroBD;
import modelo.BD.TipoSinistroBD;
import Modelo.entidade.ApoliceSeguro;
import Modelo.entidade.Sinistro;
import Modelo.entidade.TipoSinistro;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author vagne
 */
public class ControleSinistro {

    ControleSinistro tipoSinistro;
    ApoliceBD apoliceBD = new ApoliceBD();
    TipoSinistroBD tipoSinistroBD = new TipoSinistroBD();

    public String relatarSinistro(Sinistro sinistro, int codApolice) throws SQLException {
        SinistroBD s = new SinistroBD();
        return s.registrarSinistro(sinistro, codApolice);
    }

    public ArrayList<TipoSinistro> listarTiposSinistros() throws SQLException {
        return tipoSinistroBD.listarTiposSinistros();
    }
    
    public String relatarMorte(Sinistro s) {
        return null;
     }
    
    public String adicionarTipoDeSinistro(String descricao) throws SQLException {
        return tipoSinistroBD.adicionarTipoDeSinistro(descricao);
    }

    public boolean validaApolice(int codigo) throws SQLException {
        return apoliceBD.validaApolice(codigo);
    }

    public ApoliceSeguro buscaApolice(int codigo) throws SQLException {
        return apoliceBD.buscaApolice(codigo);
    }
}
