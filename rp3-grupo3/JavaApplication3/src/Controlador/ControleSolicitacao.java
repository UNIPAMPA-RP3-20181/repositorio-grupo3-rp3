/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import modelo.BD.SolicitacaoBD;
import Modelo.entidade.Candidato;
import Modelo.entidade.SolicitacaoSeguro;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author vagne
 */
public class ControleSolicitacao {

    private SolicitacaoBD solicitacaoBD;

    public ControleSolicitacao() {
        this.solicitacaoBD = new SolicitacaoBD();
    }

    public boolean avaliarSolicitacao(SolicitacaoSeguro solicitacao) {
        return false;
    }

    public boolean solicitarSeguro(SolicitacaoSeguro solicitacao) throws SQLException {
        if (solicitacao.calcularPreco()) {
            return solicitacaoBD.registrarSolicitacao(solicitacao);
        } else {
            return false;
        }
    }

    public ArrayList<SolicitacaoSeguro> listarSolicitacoesSeguro() {
        return null;
    }

    public boolean confirmarSolicitacao(SolicitacaoSeguro solicitacao) {
        return false;
    }
}
