/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.BD.SeguradoBD;
import Modelo.entidade.Contato;
import Modelo.entidade.Segurado;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author vagne
 */
public class ControleSegurado {

    SeguradoBD seguradoBD = new SeguradoBD();
    
    public ArrayList<String[]> listarSegurados(int codContato) throws SQLException {
        return seguradoBD.buscarSegurados(codContato);
    }
}
