/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.BD;

import static modelo.BD.ConexaoBD.createConnection;
import Modelo.entidade.Usuario;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author vagne
 */
public class UsuarioBD {

    public String inserirBanco(Usuario usuario) throws SQLException {
        Connection conexao = createConnection();

        String sqlPessoa = "INSERT INTO pessoa (nome,endereco,telefone,email, cpf,"
                + " cep, uf, cidade) VALUES ('" + usuario.getNome() + "','"
                + usuario.getEndereco() + "','" + usuario.getTelefone() + "','"
                + usuario.getEmail() + "', '" + usuario.getCpf() + "', '"
                + usuario.getCep() + "', '" + usuario.getUf() + "', '"
                + usuario.getCidade() + "')";
        
        String sqlUsuario = "INSERT INTO usuario (nomeLogin, senhaLogin, codPessoa) VALUES ('"
                + usuario.getNomeLogin() + "','"
                + usuario.getSenhaLogin() + "',"
                + "(SELECT codPessoa from Pessoa where cpf = " + usuario.getCpf() + "))";
//Prepara a instrução SQL
        PreparedStatement psPessoa = conexao.prepareStatement(sqlPessoa);
        PreparedStatement psUsuario = conexao.prepareStatement(sqlUsuario);
//Executa a instrução SQL

        try {
            conexao.setAutoCommit(false);
            psPessoa = conexao.prepareStatement(sqlPessoa);
            psPessoa.executeUpdate();

            psUsuario = conexao.prepareStatement(sqlUsuario);
            psUsuario.executeUpdate();

            //Grava as informações se caso de problema os dados não são gravados 
            conexao.commit();

        } catch (SQLException e) {
            if (conexao != null) {
                conexao.rollback();
                return e.getMessage();
            }
        } finally {
            if (psPessoa != null) {
                psPessoa.close();
            }
            conexao.setAutoCommit(true);
        }
        return "Usuário adicionado.";
    }
    
    public Usuario recuperaUsuario(String nomeLogin, String senhaLogin) throws SQLException {
        Connection conexao = createConnection();

        String sql = "SELECT * from pessoa p inner join usuario u on u.codPessoa = p.codPessoa where nomeLogin = '" + nomeLogin
        + "'and senhaLogin = '" + senhaLogin + "'";
        PreparedStatement ps = conexao.prepareStatement(sql);

        ResultSet rs = ps.executeQuery();
        Usuario usuario = null;
        while (rs.next()) {
            String nome = rs.getString("nome");
            String endereco = rs.getString("endereco");
            long telefone = rs.getLong("telefone");
            String email = rs.getString("email");
            long cpf = rs.getLong("cpf");
            long cep = rs.getLong("cep");
            String uf = rs.getString("uf");
            String cidade = rs.getString("cidade");
            usuario = new Usuario(nome, endereco, telefone, email, cpf, cep, uf, cidade, nomeLogin, senhaLogin);
        }
        return usuario;
    }
}
