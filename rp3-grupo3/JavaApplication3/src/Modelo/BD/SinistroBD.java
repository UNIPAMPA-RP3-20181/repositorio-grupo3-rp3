/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.BD;

import static modelo.BD.ConexaoBD.createConnection;
import Modelo.entidade.Sinistro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author vagne
 */
public class SinistroBD {

    public String registrarSinistro(Sinistro sinistro, int codApolice) throws SQLException {
        Connection conexao = createConnection();

        String sqlSinistro = "INSERT INTO sinistro (codTipoSinistro, codApolice, dataSinistro, contatoSinistro,"
                + " descricaoSinistro) VALUES (1,"+ 
                + codApolice + ",'" + sinistro.getDataSinistro()+ "','"
                + sinistro.getContatoSinistro()+ "','" + sinistro.getDescricaoSinistro()+ "')";
//Prepara a instrução SQL
        PreparedStatement psSinistro = conexao.prepareStatement(sqlSinistro);

//Executa a instrução SQL

        try {
            conexao.setAutoCommit(false);
            psSinistro = conexao.prepareStatement(sqlSinistro);
            psSinistro.executeUpdate();


            //Grava as informações se caso de problema os dados não são gravados 
            conexao.commit();

        } catch (SQLException e) {
            if (conexao != null) {
                conexao.rollback();
                return e.getMessage();
            }
        } finally {
            if (psSinistro != null) {
                psSinistro.close();
            }
            conexao.setAutoCommit(true);
        }
        return "Ok";
    }
}
