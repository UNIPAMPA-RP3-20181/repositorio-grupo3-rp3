/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.BD;

import Modelo.entidade.Segurado;
import Modelo.entidade.TipoSinistro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import static modelo.BD.ConexaoBD.createConnection;

/**
 *
 * @author vagne
 */
public class SeguradoBD {

    public ArrayList<String[]> buscarSegurados(int codContato) {
        ArrayList lista = new ArrayList<String[]>();
        try {
            Connection conexao = createConnection();
            String sql = "SELECT * from contato c"
                    + " left join segurado s on s.codContato = c.codContato"
                    + " left join candidato ca on s.codCandidato = ca.codCandidato"
                    + " left join usuario u on ca.codUsuario = u.codUsuario"
                    + " left join pessoa p on u.codPessoa = p.codPessoa"
                    + " where c.codContato = " + 0;
            System.out.println(sql);
            PreparedStatement ps = conexao.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            System.out.println(rs.first());

            while (rs.next()) {
                String nome = rs.getString("nome");
                String cpf = rs.getString("cpf");
                String[] dados = {nome, cpf};
                lista.add(dados);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lista;
    }
}
