/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.BD;

import static modelo.BD.ConexaoBD.createConnection;
import Modelo.entidade.ApoliceSeguro;
import Modelo.entidade.Segurado;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author vagne
 */
public class ApoliceBD {

    public boolean validaApolice(int codigo) throws SQLException {
        Connection conexao = createConnection();

        String sql = "SELECT * from apoliceseguro a" + " where codApolice = " + codigo + "";
        PreparedStatement ps = conexao.prepareStatement(sql);

        ResultSet rs = ps.executeQuery();
        ApoliceSeguro apolice = null;
        return rs.next();
    }

 // FAZER A BUSCA FUTURAMENTE SE FOR NECESSÁRIO \/
    public ApoliceSeguro buscaApolice(int codigo) throws SQLException {
        Connection conexao = createConnection();

        String sql = "SELECT * from apoliceseguro a "
                + "inner join segurado s on s.codSegurado = a.codSegurado "
                + "inner join candidato c on s.codCandidato = c.codCandidato "
                + "inner join usuario u on c.codUsuario = u.codUsuario "
                + "inner join pessoa p on u.codPessoa = p.codPessoa "
                + "where codApolice = '" + codigo + "'";
        System.out.println(sql);
        PreparedStatement ps = conexao.prepareStatement(sql);

        ResultSet rs = ps.executeQuery();
        ApoliceSeguro apolice = null;
        while (rs.next()) {
            Date dataContratacaoApolice = rs.getDate("dataContratacaoApolice");
            String premioApolice = rs.getString("premioApolice");
            double valorSinistroMorte = rs.getDouble("valorSinistroMorte");
            Segurado segurado = new Segurado(rs.getBoolean("vivoSegurado"), rs.getString("incapacitacaoSegurado"),
                    rs.getString("bairroCandidato"), rs.getDate("dataNascimentoCandidato"),rs.getString("sexoCandidato"),
                    rs.getString("problemasSaudeCandidato"), rs.getInt("idade"), rs.getString("nomeLogin"), 
                    rs.getString("senhaLogin"), rs.getString("nome"), rs.getString("endereco"), rs.getLong("telefone"),
                    rs.getString("email"), rs.getLong("cpf"), rs.getLong("cep"), rs.getString("uf"), rs.getString("cidade"),
                    rs.getInt("codUsuario"));
            int numeroCartao = rs.getInt("numeroCartao");
            String bandeiraCartao = rs.getString("uf");
            String nomeCartao = rs.getString("cidade");
            Date vencimentoCartao = rs.getDate("vencimentoCartao");
            int cvvCartao = rs.getInt("cvvCartao");
            apolice = new ApoliceSeguro(codigo, dataContratacaoApolice, premioApolice, valorSinistroMorte, segurado, numeroCartao, bandeiraCartao, nomeCartao, vencimentoCartao, cvvCartao);
        }
        return apolice;
    }
}
