/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.BD;

import Modelo.entidade.SolicitacaoSeguro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import static modelo.BD.ConexaoBD.createConnection;

/**
 *
 * @author vagne
 */
public class SolicitacaoBD {
    
    public boolean registrarSolicitacao (SolicitacaoSeguro solicitacao) throws SQLException{
          Connection conexao = createConnection();

        String sqlSolicitacaoSeguro = "INSERT INTO solicitacaoseguro (codCandidato, valorSolicitacao, dataSolicitacao,"
                + " valorCorrigidoSolicitacao, predisposicaoCancer, predisposicaoDiabetes, predisposicaoDemencia,"
                + " predisposicaoCoracao, predisposicaoCerebral, predisposicaoHipertensao, predisposicaoPulmonar,"
                + " predisposicaoOsteoporose, predisposicaoDegeneracao, fumante, alcoolista) VALUES (" + solicitacao.getCandidato().getCodCandidato() + ","
                + solicitacao.getValorSolicitacao() + "," + solicitacao.getDataSolicitacaoStr() + "," + solicitacao.getValorCorrigidoSolicitacao() + ","
                + solicitacao.isPredisposicaoCancer() + "," + solicitacao.isPredisposicaoDiabetes() + "," + solicitacao.isPredisposicaoDemencia() + ","
                + solicitacao.isPredisposicaoCoracao() + "," + solicitacao.isPredisposicaoCerebral() + "," + solicitacao.isPredisposicaoHipertensao() +","
                + solicitacao.isPredisposicaoPulmonar() + "," + solicitacao.isPredisposicaoOsteoporose() + "," + solicitacao.isPredisposicaoDegeneracao() + ","
                + solicitacao.isFumante() + "," + solicitacao.isAlcoolista() + ");";
        
        PreparedStatement psSolicitacao = conexao.prepareStatement(sqlSolicitacaoSeguro);
//Executa a instrução SQL

        try {
            conexao.setAutoCommit(false);
            psSolicitacao.executeUpdate();

            //Grava as informações se caso de problema os dados não são gravados 
            conexao.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            if (conexao != null) {
                conexao.rollback();
                return false;
            }
        } finally {
            if (psSolicitacao != null) {
                psSolicitacao.close();
            }
            conexao.setAutoCommit(true);
        }
        return true;
    }
    
    public boolean confirmarSolicitacao(SolicitacaoSeguro solicitacao) {
        return false;
    }
}
