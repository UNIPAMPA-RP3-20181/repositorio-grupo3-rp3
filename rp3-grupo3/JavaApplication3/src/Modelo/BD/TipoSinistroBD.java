/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.BD;

import static modelo.BD.ConexaoBD.createConnection;
import Modelo.entidade.TipoSinistro;
import Modelo.entidade.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author vagne
 */
public class TipoSinistroBD {

    public ArrayList<TipoSinistro> listarTiposSinistros() throws SQLException {
        ArrayList lista = new ArrayList<TipoSinistro>();
        Connection conexao = createConnection();

        String sql = "SELECT * from tipoSinistro";
        PreparedStatement ps = conexao.prepareStatement(sql);

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            String descricaoTipoSinistro = rs.getString("descricaoTipoSinistro");
            lista.add(new TipoSinistro(descricaoTipoSinistro));
        }
        return lista;
    }

    public String adicionarTipoDeSinistro(String descricao) throws SQLException {
         Connection conexao = createConnection();

        String sqlTipoSinistro = "INSERT INTO tiposinistro (descricaoTipoSinistro) VALUES ('" + descricao + "')";
       
        PreparedStatement psTipoSinistro = conexao.prepareStatement(sqlTipoSinistro);
//Executa a instrução SQL

        try {
            conexao.setAutoCommit(false);
            psTipoSinistro.executeUpdate();

            //Grava as informações se caso de problema os dados não são gravados 
            conexao.commit();

        } catch (SQLException e) {
            if (conexao != null) {
                conexao.rollback();
                return e.getMessage();
            }
        } finally {
            if (psTipoSinistro != null) {
                psTipoSinistro.close();
            }
            conexao.setAutoCommit(true);
        }
        return "Usuário adicionado.";
    }
}
