/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.entidade;

/**
 *
 * @author vagne
 */
public class Contato extends Usuario {

    String formaDeContatoPreferido;
    int codContato;

    public Contato(String formaDeContatoPreferido, int codContato, 
            String nome, String endereco, long telefone, String email,
            long cpf, long cep, String uf, String cidade, String nomeLogin, String senhaLogin) {
        super(nome, endereco, telefone, email, cpf, cep, uf, cidade, nomeLogin, senhaLogin);
    }
    
    public int getCodContato() {
        return codContato;
    }
}
