/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.entidade;

import java.util.Date;

/**
 *
 * @author vagne
 */
public class Segurado extends Candidato {

    public Segurado(boolean vivoSegurado, String incapacitacaoSegurado, String bairroCandidato,
            Date dataNascimentoCandidato, String sexoCandidato, String problemasSaudeCandidato, int idade,
            String nomeLogin, String senhaLogin, String nome, String endereco, long telefone,
            String email, long cpf, long cep, String uf, String cidade, int codCandidato) {
        super(bairroCandidato, dataNascimentoCandidato, sexoCandidato, problemasSaudeCandidato, idade,
                nomeLogin, senhaLogin, nome, endereco, telefone, email, cpf, cep, uf, cidade, codCandidato);
        this.vivoSegurado = vivoSegurado;
        this.incapacitacaoSegurado = incapacitacaoSegurado;
    }

    boolean vivoSegurado;
    String incapacitacaoSegurado;

    public void realizarPagamento() {
    }
}
