/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.entidade;

/**
 *
 * @author vagne
 */
public class Usuario extends Pessoa {

    String nomeLogin;
    String senhaLogin;

    public Usuario(String nome, String endereco, long telefone, String email, long cpf, long cep, String uf, String cidade, String nomeLogin, String senhaLogin) {
        super(nome, endereco, telefone, email, cpf, cep, uf, cidade);
        this.nomeLogin = nomeLogin;
        this.senhaLogin = senhaLogin;
    }

    public String getNomeLogin() {
        return nomeLogin;
    }

    public String getSenhaLogin() {
        return senhaLogin;
    }

}
