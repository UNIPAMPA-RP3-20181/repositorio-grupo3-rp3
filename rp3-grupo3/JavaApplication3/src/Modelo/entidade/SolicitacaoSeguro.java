/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.entidade;

import java.util.Date;

/**
 *
 * @author vagne
 */
public class SolicitacaoSeguro {

    public SolicitacaoSeguro(double valorSolicitacao, Date dataSolicitacao, String motivoReprovacao, Date dataVisitaCandidato, boolean aprovadaSolicitacao, String alteracoesSolicitacao, double valorCorrigidoSolicitacao, boolean predisposicaoCancer, boolean predisposicaoDiabetes, boolean predisposicaoDemencia, boolean predisposicaoCoracao, boolean predisposicaoCerebral, boolean predisposicaoHipertensao, boolean predisposicaoPulmonar, boolean predisposicaoOsteoporose, boolean predisposicaoDegeneracao, boolean fumante, boolean alcoolista, Candidato candidato) {
        this.valorSolicitacao = valorSolicitacao;
        this.dataSolicitacao = dataSolicitacao;
        this.motivoReprovacao = motivoReprovacao;
        this.dataVisitaCandidato = dataVisitaCandidato;
        this.aprovadaSolicitacao = aprovadaSolicitacao;
        this.alteracoesSolicitacao = alteracoesSolicitacao;
        this.valorCorrigidoSolicitacao = valorCorrigidoSolicitacao;
        this.predisposicaoCancer = predisposicaoCancer;
        this.predisposicaoDiabetes = predisposicaoDiabetes;
        this.predisposicaoDemencia = predisposicaoDemencia;
        this.predisposicaoCoracao = predisposicaoCoracao;
        this.predisposicaoCerebral = predisposicaoCerebral;
        this.predisposicaoHipertensao = predisposicaoHipertensao;
        this.predisposicaoPulmonar = predisposicaoPulmonar;
        this.predisposicaoOsteoporose = predisposicaoOsteoporose;
        this.predisposicaoDegeneracao = predisposicaoDegeneracao;
        this.fumante = fumante;
        this.alcoolista = alcoolista;
        this.candidato = candidato;
    }

    double valorSolicitacao;

    public double getValorSolicitacao() {
        return valorSolicitacao;
    }

    public void setValorSolicitacao(double valorSolicitacao) {
        this.valorSolicitacao = valorSolicitacao;
    }

    public Date getDataSolicitacao() {
        return dataSolicitacao;
    }
    
    public String getDataSolicitacaoStr() {
        return dataSolicitacao.getYear()+ "-" + dataSolicitacao.getMonth() + "-" + dataSolicitacao.getDay();
    }

    public void setDataSolicitacao(Date dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public String getMotivoReprovacao() {
        return motivoReprovacao;
    }

    public void setMotivoReprovacao(String motivoReprovacao) {
        this.motivoReprovacao = motivoReprovacao;
    }

    public Date getDataVisitaCandidato() {
        return dataVisitaCandidato;
    }

    public void setDataVisitaCandidato(Date dataVisitaCandidato) {
        this.dataVisitaCandidato = dataVisitaCandidato;
    }

    public boolean isAprovadaSolicitacao() {
        return aprovadaSolicitacao;
    }

    public void setAprovadaSolicitacao(boolean aprovadaSolicitacao) {
        this.aprovadaSolicitacao = aprovadaSolicitacao;
    }

    public String getAlteracoesSolicitacao() {
        return alteracoesSolicitacao;
    }

    public void setAlteracoesSolicitacao(String alteracoesSolicitacao) {
        this.alteracoesSolicitacao = alteracoesSolicitacao;
    }

    public double getValorCorrigidoSolicitacao() {
        return valorCorrigidoSolicitacao;
    }

    public void setValorCorrigidoSolicitacao(double valorCorrigidoSolicitacao) {
        this.valorCorrigidoSolicitacao = valorCorrigidoSolicitacao;
    }

    public boolean isPredisposicaoCancer() {
        return predisposicaoCancer;
    }

    public void setPredisposicaoCancer(boolean predisposicaoCancer) {
        this.predisposicaoCancer = predisposicaoCancer;
    }

    public boolean isPredisposicaoDiabetes() {
        return predisposicaoDiabetes;
    }

    public void setPredisposicaoDiabetes(boolean predisposicaoDiabetes) {
        this.predisposicaoDiabetes = predisposicaoDiabetes;
    }

    public boolean isPredisposicaoDemencia() {
        return predisposicaoDemencia;
    }

    public void setPredisposicaoDemencia(boolean predisposicaoDemencia) {
        this.predisposicaoDemencia = predisposicaoDemencia;
    }

    public boolean isPredisposicaoCoracao() {
        return predisposicaoCoracao;
    }

    public void setPredisposicaoCoracao(boolean predisposicaoCoracao) {
        this.predisposicaoCoracao = predisposicaoCoracao;
    }

    public boolean isPredisposicaoCerebral() {
        return predisposicaoCerebral;
    }

    public void setPredisposicaoCerebral(boolean predisposicaoCerebral) {
        this.predisposicaoCerebral = predisposicaoCerebral;
    }

    public boolean isPredisposicaoHipertensao() {
        return predisposicaoHipertensao;
    }

    public void setPredisposicaoHipertensao(boolean predisposicaoHipertensao) {
        this.predisposicaoHipertensao = predisposicaoHipertensao;
    }

    public boolean isPredisposicaoPulmonar() {
        return predisposicaoPulmonar;
    }

    public void setPredisposicaoPulmonar(boolean predisposicaoPulmonar) {
        this.predisposicaoPulmonar = predisposicaoPulmonar;
    }

    public boolean isPredisposicaoOsteoporose() {
        return predisposicaoOsteoporose;
    }

    public void setPredisposicaoOsteoporose(boolean predisposicaoOsteoporose) {
        this.predisposicaoOsteoporose = predisposicaoOsteoporose;
    }

    public boolean isPredisposicaoDegeneracao() {
        return predisposicaoDegeneracao;
    }

    public void setPredisposicaoDegeneracao(boolean predisposicaoDegeneracao) {
        this.predisposicaoDegeneracao = predisposicaoDegeneracao;
    }

    public boolean isFumante() {
        return fumante;
    }

    public void setFumante(boolean fumante) {
        this.fumante = fumante;
    }

    public boolean isAlcoolista() {
        return alcoolista;
    }

    public void setAlcoolista(boolean alcoolista) {
        this.alcoolista = alcoolista;
    }
    
    Date dataSolicitacao;
    String motivoReprovacao;
    Date dataVisitaCandidato;
    boolean aprovadaSolicitacao;
    String alteracoesSolicitacao;
    double valorCorrigidoSolicitacao;
    boolean predisposicaoCancer;
    boolean predisposicaoDiabetes;
    boolean predisposicaoDemencia;
    boolean predisposicaoCoracao;
    boolean predisposicaoCerebral;
    boolean predisposicaoHipertensao;
    boolean predisposicaoPulmonar;
    boolean predisposicaoOsteoporose;
    boolean predisposicaoDegeneracao;
    boolean fumante;
    boolean alcoolista;
    Candidato candidato;

    public double calcularValorBase() {
        double valorBase = 0;
        switch (candidato.sexoCandidato) {
            case "M":
                if (candidato.idade >= 18 && candidato.idade <= 25) {
                    valorBase = 617.00;
                } else if (candidato.idade >= 26 && candidato.idade <= 35) {
                    valorBase = 585.80;
                } else if (candidato.idade >= 36 && candidato.idade <= 45) {
                    valorBase = 512.95;
                } else if (candidato.idade >= 46 && candidato.idade <= 50) {
                    valorBase = 557.00;
                } else if (candidato.idade >= 51 && candidato.idade <= 55) {
                    valorBase = 589.94;
                } else if (candidato.idade >= 56 && candidato.idade <= 60) {
                    valorBase = 624.90;
                } else if (candidato.idade >= 61 && candidato.idade <= 65) {
                    valorBase = 656.66;
                } else if (candidato.idade >= 66 && candidato.idade <= 75) {
                    valorBase = 687.00;
                } else if (candidato.idade >= 76 && candidato.idade <= 85) {
                    valorBase = 714.32;
                } else if (candidato.idade >= 85 && candidato.idade <= 90) {
                    valorBase = 851.60;
                }
                break;
            case "F":
                if (candidato.idade >= 18 && candidato.idade <= 25) {
                    valorBase = 535.50;
                } else if (candidato.idade >= 26 && candidato.idade <= 35) {
                    valorBase = 516.20;
                } else if (candidato.idade >= 36 && candidato.idade <= 45) {
                    valorBase = 478.95;
                } else if (candidato.idade >= 46 && candidato.idade <= 50) {
                    valorBase = 482.17;
                } else if (candidato.idade >= 51 && candidato.idade <= 55) {
                    valorBase = 488.90;
                } else if (candidato.idade >= 56 && candidato.idade <= 60) {
                    valorBase = 524.30;
                } else if (candidato.idade >= 61 && candidato.idade <= 65) {
                    valorBase = 544.41;
                } else if (candidato.idade >= 66 && candidato.idade <= 75) {
                    valorBase = 581.50;
                } else if (candidato.idade >= 76 && candidato.idade <= 85) {
                    valorBase = 644.12;
                } else if (candidato.idade >= 85 && candidato.idade <= 90) {
                    valorBase = 792.70;
                }
                break;
            default:
                System.out.println("Não foi possível determinar o sexo desta pessoa.");
                break;
        }
        return valorBase;
    }

    public double calcularValorPredisposicoes() {
        double valorAcrescimo = 0;
        int quantidadePredisposicoes = 0;

        if (predisposicaoCancer) {
            quantidadePredisposicoes = quantidadePredisposicoes+1;
        }
        if (predisposicaoCerebral) {
            quantidadePredisposicoes = quantidadePredisposicoes+1;
        }
        if (predisposicaoCoracao) {
            quantidadePredisposicoes = quantidadePredisposicoes+1;
        }
        if (predisposicaoDegeneracao) {
            quantidadePredisposicoes = quantidadePredisposicoes+1;
        }
        if (predisposicaoDemencia) {
            quantidadePredisposicoes = quantidadePredisposicoes+1;
        }
        if (predisposicaoDiabetes) {
            quantidadePredisposicoes = quantidadePredisposicoes+1;
        }
        if (predisposicaoHipertensao) {
            quantidadePredisposicoes = quantidadePredisposicoes+1;
        }
        if (predisposicaoOsteoporose) {
            quantidadePredisposicoes = quantidadePredisposicoes+1;
        }
        if (predisposicaoPulmonar) {
            quantidadePredisposicoes = quantidadePredisposicoes+1;
        }
        valorAcrescimo = quantidadePredisposicoes * 31.64;

        if (quantidadePredisposicoes > 4) {
            return -1; // Criar exceção de limite de predisposições atingido
        }
        if (fumante) {
            valorAcrescimo = valorAcrescimo + 296.36;
        }
        if (alcoolista) {
            valorAcrescimo = valorAcrescimo + 203.09;
        }
        if (fumante && alcoolista && quantidadePredisposicoes > 2) {
            return -1; // Criar exceção de falha na contratação de seguro
        }
        return valorAcrescimo;
    }

    public boolean calcularPreco() {
        double valorPredisposicoes = calcularValorPredisposicoes();
        double valorBase = calcularValorBase();
        double valorTotal = 0;
        if (valorPredisposicoes >= 0 && valorBase >= 0) {
            valorTotal = valorBase + valorPredisposicoes;
            System.out.println("Preço: " + valorTotal + " Base: " +valorBase + " Pred:" +valorPredisposicoes);
        } else if (valorBase <= 0){
            return false;
        } else if (valorPredisposicoes <= 0) {
            return false;
        }
        return true;         
    }
    
        public Candidato getCandidato() {
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }
}

