/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.entidade;

import java.util.Date;

/**
 *
 * @author vagne
 */
public class Candidato extends Usuario {

    private final int codCandidato;

    public Candidato(String bairroCandidato, Date dataNascimentoCandidato, String sexoCandidato,
            String problemasSaudeCandidato, int idade, String nomeLogin, String senhaLogin, String nome,
            String endereco, long telefone, String email, long cpf, long cep, String uf, String cidade, int codCandidato) {
        super(nome, endereco, telefone, email, cpf, cep, uf, cidade, nomeLogin, senhaLogin);
        this.bairroCandidato = bairroCandidato;
        this.dataNascimentoCandidato = dataNascimentoCandidato;
        this.sexoCandidato = sexoCandidato;
        this.problemasSaudeCandidato = problemasSaudeCandidato;
        this.idade = idade;
        this.codCandidato = codCandidato;
    }

    String bairroCandidato;
    Date dataNascimentoCandidato;
    String sexoCandidato;
    String problemasSaudeCandidato;
    int idade;

    public String getBairroCandidato() {
        return bairroCandidato;
    }

    public Date getDataNascimentoCandidato() {
        return dataNascimentoCandidato;
    }

    public String getSexoCandidato() {
        return sexoCandidato;
    }

    public String getProblemasSaudeCandidato() {
        return problemasSaudeCandidato;
    }

    public int getIdade() {
        return idade;
    }

    public int getCodCandidato() {
        return codCandidato;
    }
}
