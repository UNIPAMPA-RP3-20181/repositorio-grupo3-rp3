/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.entidade;

import java.sql.Date;

/**
 *
 * @author vagne
 */
public class Sinistro {

    public Sinistro(boolean autorizadoSinistro, Date dataSinistro, double valorSinistro, String parecerAvaliadorSinistro, String contatoSinistro, String descricaoSinistro, ApoliceSeguro apolice, TipoSinistro tipoSinistro) {
        this.autorizadoSinistro = autorizadoSinistro;
        this.dataSinistro = dataSinistro;
        this.valorSinistro = valorSinistro;
        this.parecerAvaliadorSinistro = parecerAvaliadorSinistro;
        this.contatoSinistro = contatoSinistro;
        this.descricaoSinistro = descricaoSinistro;
        this.apolice = apolice;
        this.tipoSinistro = tipoSinistro;
    }

    public Sinistro(Date dataSinistro, ApoliceSeguro apolice, TipoSinistro tipoSinistro, String descricaoSinistro, int codApolice, String causaDaMorte) {
        this.dataSinistro = dataSinistro;
        this.apolice = apolice;
        this.tipoSinistro = tipoSinistro;
        this.descricaoSinistro = descricaoSinistro;
        this.causaDaMorte = causaDaMorte;
        this.codApolice = codApolice;
    }

    boolean autorizadoSinistro;
    Date dataSinistro;
    double valorSinistro;
    String parecerAvaliadorSinistro;
    String contatoSinistro;
    String descricaoSinistro;
    ApoliceSeguro apolice;
    TipoSinistro tipoSinistro;
    int codApolice;
    String causaDaMorte;

    public void setAutorizadoSinistro(boolean autorizadoSinistro) {
        this.autorizadoSinistro = autorizadoSinistro;
    }

    public void setDataSinistro(Date dataSinistro) {
        this.dataSinistro = dataSinistro;
    }

    public void setValorSinistro(double valorSinistro) {
        this.valorSinistro = valorSinistro;
    }

    public void setParecerAvaliadorSinistro(String parecerAvaliadorSinistro) {
        this.parecerAvaliadorSinistro = parecerAvaliadorSinistro;
    }

    public void setContatoSinistro(String contatoSinistro) {
        this.contatoSinistro = contatoSinistro;
    }

    public void setDescricaoSinistro(String descricaoSinistro) {
        this.descricaoSinistro = descricaoSinistro;
    }

    public void setApolice(ApoliceSeguro apolice) {
        this.apolice = apolice;
    }

    public void setTipoSinistro(TipoSinistro tipoSinistro) {
        this.tipoSinistro = tipoSinistro;
    }

    public boolean isAutorizadoSinistro() {
        return autorizadoSinistro;
    }

    public Date getDataSinistro() {
        return dataSinistro;
    }

    public double getValorSinistro() {
        return valorSinistro;
    }

    public String getParecerAvaliadorSinistro() {
        return parecerAvaliadorSinistro;
    }

    public String getContatoSinistro() {
        return contatoSinistro;
    }

    public String getDescricaoSinistro() {
        return descricaoSinistro;
    }

    public ApoliceSeguro getApolice() {
        return apolice;
    }

    public TipoSinistro getTipoSinistro() {
        return tipoSinistro;
    }

    public String getCausaDaMorte() {
        return causaDaMorte;
    }

    public void setCausaDaMorte(String causaDaMorte) {
        this.causaDaMorte = causaDaMorte;
    }
}
