/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.entidade;

import java.util.Date;

/**
 *
 * @author vagne
 */
public class ApoliceSeguro {

    public ApoliceSeguro(int codApolice, Date dataContratacaoApolice, String premioApolice, double valorSinistroMorte, Segurado segurado, int numeroCartao, String bandeiraCartao, String nomeCartao, Date vencimentoCartao, int cvvCartao) {
        this.codApolice = codApolice;
        this.dataContratacaoApolice = dataContratacaoApolice;
        this.premioApolice = premioApolice;
        this.valorSinistroMorte = valorSinistroMorte;
        this.segurado = segurado;
        this.numeroCartao = numeroCartao;
        this.bandeiraCartao = bandeiraCartao;
        this.nomeCartao = nomeCartao;
        this.vencimentoCartao = vencimentoCartao;
        this.cvvCartao = cvvCartao;
    }

    int codApolice;
    Date dataContratacaoApolice;
    String premioApolice;
    double valorSinistroMorte;
    Segurado segurado;
    int numeroCartao;
    String bandeiraCartao;
    String nomeCartao;
    Date vencimentoCartao;
    int cvvCartao;

    public int getNumeroApolice() {
        return codApolice;
    }

    public Date getDataContratacaoApolice() {
        return dataContratacaoApolice;
    }

    public String getPremioApolice() {
        return premioApolice;
    }

    public double getValorSinistroMorte() {
        return valorSinistroMorte;
    }

    public Segurado getSegurado() {
        return segurado;
    }

    public int getNumeroCartao() {
        return numeroCartao;
    }

    public String getBandeiraCartao() {
        return bandeiraCartao;
    }

    public String getNomeCartao() {
        return nomeCartao;
    }

    public Date getVencimentoCartao() {
        return vencimentoCartao;
    }

    public int getCvvCartao() {
        return cvvCartao;
    }

    public void setNumeroApolice(int numeroApolice) {
        this.codApolice = numeroApolice;
    }

    public void setDataContratacaoApolice(Date dataContratacaoApolice) {
        this.dataContratacaoApolice = dataContratacaoApolice;
    }

    public void setPremioApolice(String premioApolice) {
        this.premioApolice = premioApolice;
    }

    public void setValorSinistroMorte(long valorSinistroMorte) {
        this.valorSinistroMorte = valorSinistroMorte;
    }

    public void setSegurado(Segurado segurado) {
        this.segurado = segurado;
    }

    public void setNumeroCartao(int numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public void setBandeiraCartao(String bandeiraCartao) {
        this.bandeiraCartao = bandeiraCartao;
    }

    public void setNomeCartao(String nomeCartao) {
        this.nomeCartao = nomeCartao;
    }

    public void setVencimentoCartao(Date vencimentoCartao) {
        this.vencimentoCartao = vencimentoCartao;
    }

    public void setCvvCartao(int cvvCartao) {
        this.cvvCartao = cvvCartao;
    }
}
