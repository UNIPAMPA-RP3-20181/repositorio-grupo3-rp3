/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.entidade;

/**
 *
 * @author vagne
 */
public class Pessoa {

    String nome;
    String endereco;
    long telefone;
    String email;
    long cpf;
    long cep;
    String uf;
    String cidade;

    public Pessoa(String nome, String endereco, long telefone, String email, long cpf, long cep, String uf, String cidade) {
        this.nome = nome;
        this.endereco = endereco;
        this.telefone = telefone;
        this.email = email;
        this.cpf = cpf;
        this.cep = cep;
        this.uf = uf;
        this.cidade = cidade;
    }

    public String getNome() {
        return nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public long getTelefone() {
        return telefone;
    }

    public String getEmail() {
        return email;
    }

    public long getCpf() {
        return cpf;
    }

    public long getCep() {
        return cep;
    }

    public String getUf() {
        return uf;
    }

    public String getCidade() {
        return cidade;
    }
}
