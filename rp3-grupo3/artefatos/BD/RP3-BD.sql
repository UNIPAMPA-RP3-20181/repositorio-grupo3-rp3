CREATE DATABASE  IF NOT EXISTS `segurodevida2` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `segurodevida2`;
-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: segurodevida2
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apoliceseguro`
--

DROP TABLE IF EXISTS `apoliceseguro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `apoliceseguro` (
  `codApolice` int(11) NOT NULL AUTO_INCREMENT,
  `codSolicitacao` int(11) DEFAULT NULL,
  `codSegurado` int(11) DEFAULT NULL,
  `numeroApolice` int(11) DEFAULT NULL,
  `dataContratacaoApolice` date DEFAULT NULL,
  `premioApolice` varchar(50) DEFAULT NULL,
  `valorSinistroMorte` mediumtext,
  `numeroCartao` int(11) DEFAULT NULL,
  `bandeiraCartao` varchar(20) DEFAULT NULL,
  `nomeCartao` varchar(50) DEFAULT NULL,
  `vencimentoCartao` date DEFAULT NULL,
  `cvvCartao` int(11) DEFAULT NULL,
  PRIMARY KEY (`codApolice`),
  KEY `codSolicitacao` (`codSolicitacao`),
  KEY `codSegurado` (`codSegurado`),
  CONSTRAINT `apoliceseguro_ibfk_1` FOREIGN KEY (`codSolicitacao`) REFERENCES `solicitacaoseguro` (`codsolicitacao`),
  CONSTRAINT `apoliceseguro_ibfk_2` FOREIGN KEY (`codSegurado`) REFERENCES `segurado` (`codsegurado`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apoliceseguro`
--

LOCK TABLES `apoliceseguro` WRITE;
/*!40000 ALTER TABLE `apoliceseguro` DISABLE KEYS */;
INSERT INTO `apoliceseguro` VALUES (1,1,1,23,'2015-08-29','Recebeu dinheiro','800',36,'Visa','Vagner Quincozes','2017-09-05',842);
/*!40000 ALTER TABLE `apoliceseguro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apolicetiposinistros`
--

DROP TABLE IF EXISTS `apolicetiposinistros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `apolicetiposinistros` (
  `codApoliceTipoSinistro` int(11) NOT NULL AUTO_INCREMENT,
  `codtipoSinistro` int(11) DEFAULT NULL,
  `codApolice` int(11) DEFAULT NULL,
  PRIMARY KEY (`codApoliceTipoSinistro`),
  KEY `codtipoSinistro` (`codtipoSinistro`),
  KEY `codApolice` (`codApolice`),
  CONSTRAINT `apolicetiposinistros_ibfk_1` FOREIGN KEY (`codtipoSinistro`) REFERENCES `tiposinistro` (`codtiposinistro`),
  CONSTRAINT `apolicetiposinistros_ibfk_2` FOREIGN KEY (`codApolice`) REFERENCES `apoliceseguro` (`codapolice`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apolicetiposinistros`
--

LOCK TABLES `apolicetiposinistros` WRITE;
/*!40000 ALTER TABLE `apolicetiposinistros` DISABLE KEYS */;
INSERT INTO `apolicetiposinistros` VALUES (1,1,1);
/*!40000 ALTER TABLE `apolicetiposinistros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avaliador`
--

DROP TABLE IF EXISTS `avaliador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `avaliador` (
  `codAvaliador` int(11) NOT NULL AUTO_INCREMENT,
  `codFuncionario` int(11) DEFAULT NULL,
  PRIMARY KEY (`codAvaliador`),
  KEY `codFuncionario` (`codFuncionario`),
  CONSTRAINT `avaliador_ibfk_1` FOREIGN KEY (`codFuncionario`) REFERENCES `funcionario` (`codfuncionario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avaliador`
--

LOCK TABLES `avaliador` WRITE;
/*!40000 ALTER TABLE `avaliador` DISABLE KEYS */;
INSERT INTO `avaliador` VALUES (1,1);
/*!40000 ALTER TABLE `avaliador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beneficiario`
--

DROP TABLE IF EXISTS `beneficiario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `beneficiario` (
  `codBeneficiario` int(11) NOT NULL AUTO_INCREMENT,
  `codPessoa` int(11) DEFAULT NULL,
  `idade` int(11) DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `parentescoBeneficiario` varchar(20) DEFAULT NULL,
  `codCandidato` int(11) DEFAULT NULL,
  PRIMARY KEY (`codBeneficiario`),
  KEY `codPessoa` (`codPessoa`),
  KEY `chave_estrangeira` (`codCandidato`),
  CONSTRAINT `beneficiario_ibfk_1` FOREIGN KEY (`codPessoa`) REFERENCES `pessoa` (`codpessoa`),
  CONSTRAINT `chave_estrangeira` FOREIGN KEY (`codCandidato`) REFERENCES `candidato` (`codcandidato`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beneficiario`
--

LOCK TABLES `beneficiario` WRITE;
/*!40000 ALTER TABLE `beneficiario` DISABLE KEYS */;
INSERT INTO `beneficiario` VALUES (1,1,25,'M','Filho',1);
/*!40000 ALTER TABLE `beneficiario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidato`
--

DROP TABLE IF EXISTS `candidato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `candidato` (
  `codCandidato` int(11) NOT NULL AUTO_INCREMENT,
  `codUsuario` int(11) DEFAULT NULL,
  `bairroCandidato` varchar(50) DEFAULT NULL,
  `dataNascimentoCandidato` date DEFAULT NULL,
  `sexoCandidato` char(1) DEFAULT NULL,
  `problemasSaudeCandidato` varchar(500) DEFAULT NULL,
  `codAvaliador` int(11) DEFAULT NULL,
  `idade` int(11) DEFAULT NULL,
  PRIMARY KEY (`codCandidato`),
  KEY `codUsuario` (`codUsuario`),
  KEY `chave_estrangeira_candidato` (`codAvaliador`),
  CONSTRAINT `candidato_ibfk_1` FOREIGN KEY (`codUsuario`) REFERENCES `usuario` (`codusuario`),
  CONSTRAINT `chave_estrangeira_candidato` FOREIGN KEY (`codAvaliador`) REFERENCES `avaliador` (`codavaliador`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidato`
--

LOCK TABLES `candidato` WRITE;
/*!40000 ALTER TABLE `candidato` DISABLE KEYS */;
INSERT INTO `candidato` VALUES (1,1,'Centro','2018-05-23','M','Não possui problemas por enquanto',1,NULL),(2,7,'Centro','1976-08-25','M','Problema no joelho',NULL,44),(3,8,'Vila Isabel','1986-07-13','M','Problema no braço',NULL,32),(4,9,'Vila Inês','1984-11-27','M','Não possui',NULL,34),(5,10,'Centro','1978-01-08','M','Não possui',NULL,40);
/*!40000 ALTER TABLE `candidato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `contato` (
  `codContato` int(11) NOT NULL,
  `codUsuario` int(11) NOT NULL,
  `formaDeContatoPreferido` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codContato`),
  KEY `codUsuario` (`codUsuario`),
  CONSTRAINT `contato_ibfk_1` FOREIGN KEY (`codUsuario`) REFERENCES `usuario` (`codusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (0,1,'E-mail'),(1,2,'Contato via Telefone'),(2,3,'Contato por Whatsapp'),(3,7,'Via E-mail'),(4,8,'Via Whatsapp'),(5,9,'Via telefone residencial'),(6,10,'Via e-mail');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corretor`
--

DROP TABLE IF EXISTS `corretor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `corretor` (
  `codCorretor` int(11) NOT NULL AUTO_INCREMENT,
  `codFuncionario` int(11) DEFAULT NULL,
  PRIMARY KEY (`codCorretor`),
  KEY `codFuncionario` (`codFuncionario`),
  CONSTRAINT `corretor_ibfk_1` FOREIGN KEY (`codFuncionario`) REFERENCES `funcionario` (`codfuncionario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corretor`
--

LOCK TABLES `corretor` WRITE;
/*!40000 ALTER TABLE `corretor` DISABLE KEYS */;
INSERT INTO `corretor` VALUES (1,1);
/*!40000 ALTER TABLE `corretor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funcionario`
--

DROP TABLE IF EXISTS `funcionario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `funcionario` (
  `codFuncionario` int(11) NOT NULL AUTO_INCREMENT,
  `codUsuario` int(11) DEFAULT NULL,
  `dataContratacao` date DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`codFuncionario`),
  KEY `codUsuario` (`codUsuario`),
  CONSTRAINT `funcionario_ibfk_1` FOREIGN KEY (`codUsuario`) REFERENCES `usuario` (`codusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcionario`
--

LOCK TABLES `funcionario` WRITE;
/*!40000 ALTER TABLE `funcionario` DISABLE KEYS */;
INSERT INTO `funcionario` VALUES (1,1,'2015-01-27',1);
/*!40000 ALTER TABLE `funcionario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parcelaapolice`
--

DROP TABLE IF EXISTS `parcelaapolice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `parcelaapolice` (
  `codParcela` int(11) NOT NULL AUTO_INCREMENT,
  `codApolice` int(11) DEFAULT NULL,
  `numParcela` int(11) DEFAULT NULL,
  `valorParcela` double DEFAULT NULL,
  `dataPagamentoParcela` date DEFAULT NULL,
  `dataVencimento` date DEFAULT NULL,
  PRIMARY KEY (`codParcela`),
  KEY `codApolice` (`codApolice`),
  CONSTRAINT `parcelaapolice_ibfk_1` FOREIGN KEY (`codApolice`) REFERENCES `apoliceseguro` (`codapolice`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parcelaapolice`
--

LOCK TABLES `parcelaapolice` WRITE;
/*!40000 ALTER TABLE `parcelaapolice` DISABLE KEYS */;
INSERT INTO `parcelaapolice` VALUES (1,1,5,212,'2018-08-20','2018-09-20');
/*!40000 ALTER TABLE `parcelaapolice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pessoa` (
  `codPessoa` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) DEFAULT NULL,
  `cpf` mediumtext,
  `nome` varchar(50) DEFAULT NULL,
  `endereco` varchar(50) DEFAULT NULL,
  `telefone` mediumtext,
  `cep` mediumtext,
  `uf` varchar(2) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codPessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa`
--

LOCK TABLES `pessoa` WRITE;
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` VALUES (1,'vagnerereno@hotmail.com','50241198761','Vagner Ereno Quincozes','Praça Nova','55966966325','565465465','RS','Alegrete'),(4,'c','2','v','c','2','2','v','c'),(5,'email@email.com','40','nome','endereco','55','9775','uf','cidade'),(6,'ea','21','ee','aew','21','213','we','e'),(7,'hauhae','292','vagner','aheuiahe','2828','122','hh','ahha'),(9,'ahuhaui@hotmail.com','20292292020','Joao Aberleu','rua frederico washe','99988776622','188181818','rs','alegrete'),(10,'jorgearagao@gmail.com','40451191262','Jorge Aragão','Rua Cardozo','996230562','97760500','RS','São Borja'),(11,'jefersoncardozo@gmail.com','85251191963','Jeferson da Silva Cardozo','Rua Emilio Peters','998523562','98660620','RS','São Gabriel'),(12,'joaquimfernando@gmail.com','13248175162','Joaquim Fernando','Rua Barão do Amazonas','999856482','93326000','RS','Jaguari'),(13,'fernandoteko@gmail.com','52635986147','Fernando Tereko Teko','Assis Brasil','985632514','98650000','RS','São Francisco de Assis');
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segurado`
--

DROP TABLE IF EXISTS `segurado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `segurado` (
  `codSegurado` int(11) NOT NULL AUTO_INCREMENT,
  `codCandidato` int(11) DEFAULT NULL,
  `codContato` int(11) NOT NULL,
  `vivoSegurado` tinyint(1) DEFAULT NULL,
  `incapacitacaoSegurado` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codSegurado`),
  KEY `codCandidato` (`codCandidato`),
  KEY `contato_idx` (`codContato`),
  CONSTRAINT `segurado_ibfk_1` FOREIGN KEY (`codCandidato`) REFERENCES `candidato` (`codcandidato`),
  CONSTRAINT `segurado_ibfk_2` FOREIGN KEY (`codContato`) REFERENCES `contato` (`codcontato`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segurado`
--

LOCK TABLES `segurado` WRITE;
/*!40000 ALTER TABLE `segurado` DISABLE KEYS */;
INSERT INTO `segurado` VALUES (4,1,0,1,'Não'),(5,1,0,1,'Sim'),(6,2,0,1,'Não'),(7,3,0,1,'Sim'),(8,4,0,1,'Não'),(9,5,0,1,'Não');
/*!40000 ALTER TABLE `segurado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sinistro`
--

DROP TABLE IF EXISTS `sinistro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sinistro` (
  `codSinistro` int(11) NOT NULL AUTO_INCREMENT,
  `codTipoSinistro` int(11) DEFAULT NULL,
  `codApolice` int(11) DEFAULT NULL,
  `codAvaliador` int(11) DEFAULT NULL,
  `autorizadoSinistro` tinyint(1) DEFAULT NULL,
  `dataSinistro` date DEFAULT NULL,
  `valorSinistro` double DEFAULT NULL,
  `parecerAvaliadorSinistro` varchar(500) DEFAULT NULL,
  `contatoSinistro` varchar(50) DEFAULT NULL,
  `descricaoSinistro` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codSinistro`),
  KEY `codTipoSinistro` (`codTipoSinistro`),
  KEY `codApolice` (`codApolice`),
  KEY `codAvaliador` (`codAvaliador`),
  CONSTRAINT `sinistro_ibfk_1` FOREIGN KEY (`codTipoSinistro`) REFERENCES `tiposinistro` (`codtiposinistro`),
  CONSTRAINT `sinistro_ibfk_2` FOREIGN KEY (`codApolice`) REFERENCES `apoliceseguro` (`codapolice`),
  CONSTRAINT `sinistro_ibfk_3` FOREIGN KEY (`codAvaliador`) REFERENCES `avaliador` (`codavaliador`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sinistro`
--

LOCK TABLES `sinistro` WRITE;
/*!40000 ALTER TABLE `sinistro` DISABLE KEYS */;
INSERT INTO `sinistro` VALUES (1,1,1,1,1,'2017-09-24',525,'Parecer do avaliador indisponível','Tia Valderilia','Muitas predisposilçosessss'),(2,1,1,NULL,NULL,'1998-05-15',NULL,NULL,'null','aASD'),(5,1,1,NULL,NULL,'2018-05-25',NULL,NULL,'null','i'),(6,1,1,NULL,NULL,'2018-07-11',NULL,NULL,'null','Morreu'),(7,1,1,NULL,NULL,'2018-05-15',NULL,NULL,'null','xsd');
/*!40000 ALTER TABLE `sinistro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitacaoseguro`
--

DROP TABLE IF EXISTS `solicitacaoseguro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `solicitacaoseguro` (
  `codSolicitacao` int(11) NOT NULL AUTO_INCREMENT,
  `codCandidato` int(11) DEFAULT NULL,
  `valorSolicitacao` double DEFAULT NULL,
  `dataSolicitacao` date DEFAULT NULL,
  `motivoReprovacao` varchar(300) DEFAULT NULL,
  `dataVisitaCandidato` date DEFAULT NULL,
  `aprovadaSolicitacao` tinyint(1) DEFAULT NULL,
  `alteracoesSolicitacao` varchar(300) DEFAULT NULL,
  `valorCorrigidoSolicitacao` double DEFAULT NULL,
  `predisposicaoCancer` tinyint(1) DEFAULT NULL,
  `predisposicaoDiabetes` tinyint(1) DEFAULT NULL,
  `predisposicaoDemencia` tinyint(1) DEFAULT NULL,
  `predisposicaoCoracao` tinyint(1) DEFAULT NULL,
  `predisposicaoCerebral` tinyint(1) DEFAULT NULL,
  `predisposicaoHipertensao` tinyint(1) DEFAULT NULL,
  `predisposicaoPulmonar` tinyint(1) DEFAULT NULL,
  `predisposicaoOsteoporose` tinyint(1) DEFAULT NULL,
  `predisposicaoDegeneracao` tinyint(1) DEFAULT NULL,
  `fumante` tinyint(1) DEFAULT NULL,
  `alcoolista` tinyint(1) DEFAULT NULL,
  `codCorretor` int(11) DEFAULT NULL,
  PRIMARY KEY (`codSolicitacao`),
  KEY `codCandidato` (`codCandidato`),
  CONSTRAINT `solicitacaoseguro_ibfk_1` FOREIGN KEY (`codCandidato`) REFERENCES `candidato` (`codcandidato`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitacaoseguro`
--

LOCK TABLES `solicitacaoseguro` WRITE;
/*!40000 ALTER TABLE `solicitacaoseguro` DISABLE KEYS */;
INSERT INTO `solicitacaoseguro` VALUES (1,1,400,'2016-04-25','Tem muitas predisposições','2016-04-28',1,'Não tem',452,1,1,1,1,0,0,0,0,0,0,0,1),(2,1,0,'2000-01-08',NULL,NULL,NULL,NULL,0,0,0,0,1,1,0,0,0,0,0,0,NULL),(3,1,0,'2000-01-08',NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,1,0,NULL);
/*!40000 ALTER TABLE `solicitacaoseguro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiposinistro`
--

DROP TABLE IF EXISTS `tiposinistro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiposinistro` (
  `codTipoSinistro` int(11) NOT NULL AUTO_INCREMENT,
  `descricaoTipoSinistro` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codTipoSinistro`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiposinistro`
--

LOCK TABLES `tiposinistro` WRITE;
/*!40000 ALTER TABLE `tiposinistro` DISABLE KEYS */;
INSERT INTO `tiposinistro` VALUES (1,'é um sinistro de tal forma'),(2,'Morte Natural'),(3,'Morte não natural'),(4,'Acidente');
/*!40000 ALTER TABLE `tiposinistro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `usuario` (
  `codUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `codPessoa` int(11) DEFAULT NULL,
  `nomeLogin` varchar(50) DEFAULT NULL,
  `senhaLogin` varchar(256) DEFAULT NULL,
  `nome` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codUsuario`),
  KEY `codPessoa` (`codPessoa`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`codPessoa`) REFERENCES `pessoa` (`codpessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,1,'vagnerereno','vagner00','Vagner Ereno Quincozes'),(2,4,'f','f',NULL),(3,5,'vagnerr','vagnerr',NULL),(4,6,'q','q',NULL),(5,7,'vagner10','vagner10',NULL),(6,9,'joaoalberto123','joao123',NULL),(7,10,'jorgearagao','jorgearagao','Jorge Aragão'),(8,11,'jefersoncardozo','jefersoncardozo','Jeferson Cardozo'),(9,12,'joaquimfernando','joaquimfernando','Joaquim Fernando'),(10,13,'fernandoteko','fernandoteko','Fernando Teko');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-13  0:12:22
